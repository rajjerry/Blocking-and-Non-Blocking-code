var fs = require('fs');
console.log('File reading started');
fs.readFile('readFile.txt', (error, data) => {
    if (error) console.log('Error : ', error);
    console.log('File reading ended');
});
callAnotherMethod();

function callAnotherMethod() {
    console.log('This method call while file is reading. SO its non-blocking');
}